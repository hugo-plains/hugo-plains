# Plains

A lightweight and accessible Hugo theme with minimalistic design.

## Setup | Docs | Guide

Detailed documentation, configuration options, and copy-pasta instructions are available in the [exampleSite/config.toml](//gitlab.com/hugo-plains/hugo-plains/-/blob/plains/exampleSite/config.toml) file. Everything is explained where it is configured.

### If you know what you are doing:
```bash
git submodule add https://gitlab.com/hugo-plains/hugo-plains.git themes/plains
cp themes/plains/exampleSite/config.toml config.toml
```

## Overview

[exampleSite]() is available at [hugo-plains.gitlab.io/hugo-plains](//hugo-plains.gitlab.io/hugo-plains)

***

[![ALT TEXT NOT AVAILABLE](images/screenshot.png)](//hugo-plains.gitlab.io/hugo-plains)

### Features

 - Dark / Light theme, optionally based on the user's sunrise/-set times. Visitor's dark-mode settings make the site always appear in dark mode.
 - #14

***

[@nanxiaobei](//github.com/nanxiaobei)'s [Paper](//github.com/nanxiaobei/hugo-paper) theme was independently forked by [@jtagcat](//gitlab.com/jtagcat) and [@kdkasad](//gitlab.com/kdkasad). The forks were later merged to, and renamed as Plains.

Plains still considers Paper as upstream. Most commits reach downstream, yet few upstream.