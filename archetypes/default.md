---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
excludeFromListing: false # Setting this to true hides the page/post from *all* list pages.
draft: true
---

